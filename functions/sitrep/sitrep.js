require('dotenv').config();

const fetch = require('node-fetch');

exports.handler = function(event) {
	// Check we are being called from the same site
	if(event.headers['sec-fetch-site'] !== 'same-origin') {
		return {
			statusCode: 401,
			body: 'You are not authorized to access this function'
		};
	}

	// Check we have an action in the URL
	if(!event.queryStringParameters.action) {
		return {
			statusCode: 400,
			body: 'The "action" parameter is missing'
		};
	}

	// Build the initial payload
	const payload = {
		method: 'GET',
		headers: {
			'content-type': 'application/json; charset=utf-8',
			'authorization': `Bearer ${process.env.SLACK_API_TOKEN}`
		}
	};

	// Set the default endpoint as read
	let endpoint = 'users.profile.get';

	// Override if we want to update
	if(event.queryStringParameters.action == 'update') {

		endpoint = 'users.profile.set';

		payload['method'] = 'POST';
		// Geneate a predefined status based on a code
		// 0 = clear, 1 = lunch, 2 = call, 3 = afk, 4 = helping with kids
		payload['body'] = JSON.stringify(
			generateStatus(event.queryStringParameters.statusCode)
		);
	}

	// Actually do the thing
	return fetch(
		`https://slack.com/api/${endpoint}`,
		payload
	)
		// Parse the response as JSON
		.then(data => data.json())
		// If we get it right
		.then((data) => ({
			statusCode: 200,
			body: JSON.stringify(data)
		}))
		// If we don't
		.catch(error => ({
			statusCode: 422,
			body: `Oops! Something went wrong. ${error}`
		}));
};

// Generate the status
function generateStatus(code) {
	let emoji,
		text;

	// Parse the input as a number
	switch (parseInt(code)) {
		// Lunch
		case 1:
			emoji = ':sandwich:';
			text = 'Having lunch';
			break;
		// In a call
		case 2:
			emoji = ':phone:';
			text = 'On a call';
			break;
		// AFK
		case 3:
			emoji = ':brb:';
			text = 'Away from keyboard';
			break;

		// Helping with kids
		case 4:
			emoji = ':kids:';
			text = 'Helping with the boy';
			break;

		default:
			emoji = '';
			text = '';
			break;
	}

	return {
		profile: {
			status_text: text,
			status_emoji: emoji,
			status_expiration: 0
		}
	};
}