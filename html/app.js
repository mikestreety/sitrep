document.addEventListener('click', function(e){
	if(e.target && Object.keys(e.target.dataset).length && e.target.dataset.status.length) {
		setStatus(e.target.dataset.status);
	}
});

getCurrentStatus();
function setStatus(code) {
	getJSON('/.netlify/functions/sitrep?action=update&statusCode=' + code)
		.then(() => getCurrentStatus());
}

function getCurrentStatus() {
	storeCurrentStatus()
		.then(status => {
			const current = document.getElementById('currentStatus');
			current.innerText = `${emoji(status.profile.status_emoji)} ${status.profile.status_text}`;
		});
}

function storeCurrentStatus() {
	return getJSON('/.netlify/functions/sitrep?action=get')
		.then(data => {
			localStorage.setItem('sitrep', JSON.stringify(data));
			return data;
		});
}

function getJSON(url) {
	return fetch(url)
		.then(data => data.json());
}

function emoji(input) {
	let output = '';

	let emojimap = {
		':sandwich:': '🥪',
		':phone:': '☎️',
		':brb:': '↪️',
		':kids:': '🧒'
	};

	if(emojimap.hasOwnProperty(input)) {
		output = emojimap[input];
	}

	return output;
}